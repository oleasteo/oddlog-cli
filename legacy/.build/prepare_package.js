#!/usr/bin/env node

"use strict";

const fs = require("fs");
const path = require("path");

const BASE_DIR = path.resolve(__dirname, "..", "..");
const LEGACY_DIR = path.resolve(__dirname, "..");

fs.readFile(path.join(BASE_DIR, "package.json"), (err, content) => {
  if (err != null) { return process.exit(1); }
  let pkg = JSON.parse(content);

  pkg.name += "-legacy";
  pkg.description = pkg.description.replace(/(\.?)$/, " - Legacy version$1");
  pkg.engines = {node: ">=0.12.0 <6.0.0"};
  pkg.bin["oddlog-legacy"] = pkg.bin.oddlog;
  Reflect.deleteProperty(pkg.bin, "oddlog");
  Reflect.deleteProperty(pkg, "scripts");
  Reflect.deleteProperty(pkg, "devDependencies");

  fs.writeFile(path.join(LEGACY_DIR, "package.json"), JSON.stringify(pkg, null, 2), (err) => {
    if (err != null) { return process.exit(1); }
  });
});
