# oddlog CLI Legacy

[![License](https://img.shields.io/npm/l/oddlog-cli-legacy.svg)](LICENSE)
[![Version](https://img.shields.io/npm/v/oddlog-cli-legacy.svg)](https://www.npmjs.com/package/oddlog-cli-legacy)
[![build status](https://gitlab.com/frissdiegurke/oddlog-cli/badges/master/build.svg)](https://gitlab.com/frissdiegurke/oddlog-cli/commits/master)
[![Downloads](https://img.shields.io/npm/dm/oddlog-cli-legacy.svg)](https://www.npmjs.com/package/oddlog-cli-legacy)

This is the legacy variant (babel transpiled code) of [`oddlog-cli`](https://www.npmjs.com/package/oddlog-cli).

See [https://gitlab.com/frissdiegurke/oddlog-cli](https://gitlab.com/frissdiegurke/oddlog-cli) for usage instructions.
The legacy variant binary is called `oddlog-legacy` instead of `oddlog`.
