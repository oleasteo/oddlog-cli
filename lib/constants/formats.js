/* eslint no-sync: "off" */
"use strict";

const fs = require("fs");
const path = require("path");

const LIST = [];
const DEFAULT = "pretty";

/*===================================================== Exports  =====================================================*/

exports.LIST = LIST;
exports.DEFAULT = DEFAULT;

/*================================================ Initial Execution  ================================================*/

let files = fs.readdirSync(path.join(__dirname, "..", "services", "formatter"));

for (let i = 0; i < files.length; i++) {
  let file = files[i];
  if (file.endsWith(".js")) { LIST.push(path.basename(file, ".js")); }
}
