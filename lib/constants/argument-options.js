"use strict";

const os = require("os");

const {LIST: LEVELS} = require("./levels");
const styles = require("./styles");
const formats = require("./formats");

const verbosityGroup = [];

/*===================================================== Exports  =====================================================*/

exports.commands = {
  l: {
    alias: "level",
    describe: "Filter messages with given level and above.",
    string: true,
    nargs: 1
  },
  d: {
    alias: "date",
    describe: "Filter messages by date or time. The accepted format is based on the ISO format " +
    "YYYY-MM-DDTHH:mm:ss.sTZD. Parts of this can be omitted. The T can be replaced with whitespace as well. If no " +
    "TZD is provided, local timezone will be used. Valid examples include '16-1-1' (anything on the 1st January of " +
    "**16), '1 12Z' (anything on the 1st of any month at [12:00,13:00) in UTC) and 'T12:0+2:4' (anything on any day " +
    "at [12:00,13:00) in timezone +02:04).",
    string: true,
    nargs: 1
  },
  a: {
    alias: ["application", "name"],
    describe: "Filter messages by application name. The ns-matcher package is used for matching; See " +
    "https://www.npmjs.com/package/ns-matcher#examples . This allows advanced and multiple (ordered) glob patterns. " +
    "This option can also be set via the environment variable DEBUG.",
    array: true
  },
  // f: {
  //   alias: "filter",
  //   describe: "Filter messages by evaluating javascript expression. The this variable points to the current " +
  //   "message. You may access INFO, etc. for their number value.",
  //   array: true
  // },
  // r: {
  //   alias: "reject",
  //   describe: "Filter messages by rejecting via an evaluated javascript expression. See -f for further information.",
  //   array: true
  // },
  m: {
    alias: "module",
    describe: "Use a node.js module to modify behavior.", // TODO specify details within README.md and add reference
    nargs: 1,
    normalize: true
  },
  v: {
    alias: "verbose",
    describe: "Increase verbosity for all levels that ain't overwritten by level specific verbosity options. This is " +
    "equal to set verbosity-silent, but with counted value.",
    count: true
  },
  e: {
    alias: "encoding",
    describe: "Set encoding format.",
    default: "utf8"
  },
  i: {
    alias: "invalid",
    describe: "Pass lines that cannot be interpreted as oddlog messages to stdout.",
    boolean: true
  },
  o: {
    alias: "format",
    describe: "Select a pre-defined format.",
    choices: formats.LIST,
    default: formats.DEFAULT
  },
  s: {
    alias: "style",
    describe: "The style variant for the format.",
    choices: styles.LIST
  },
  "iso-date": {
    describe: "Use the ISO date format.",
    boolean: true
  },
  n: {
    alias: ["newline"],
    describe: "Separate records with an empty line.",
    boolean: true
  },
  FS: {
    alias: "field-separator",
    describe: "Specify a field-separator for csv output",
    default: ","
  },
  LS: {
    alias: ["line-separator", "EOL"],
    describe: "Specify a line-separator.",
    default: os.EOL
  }
};

for (let i = 0; i < LEVELS.length; i++) {
  let levelName = LEVELS[i];
  exports.commands[levelName[0].toUpperCase()] = {
    alias: "verbosity-" + levelName,
    describe: "Set the verbosity of the level " + levelName.toUpperCase() + " and above.",
    nargs: 1,
    number: true
  };
  verbosityGroup.push("verbosity-" + levelName);
}

exports.groups = [
  {commands: ["level", "name", "date"], describe: "Message filter options:"},
  {
    commands: ["encoding", "verbose", "invalid", "format", "style", "iso-date", "newline", "EOL"],
    describe: "Output options:"
  },
  {commands: ["field-separator"], describe: "CSV format options:"},
  {commands: verbosityGroup, describe: "Level specific verbosity options:"}
];

exports.examples = [
  {command: "tail -f logs/my-logs.log | $0", describe: "Output formatted logs as they get written."},
  {
    command: "cat log | $0 -a 'myApp/*' '!myApp/void'",
    describe: "Filter for messages with logger name prefix myApp/ but not myApp/void."
  },
  {
    command: "cat log | $0 -d '2016-12-14T16:20:45.895Z' -vvvvv",
    describe: "Filter for messages with exact iso date match. Display with highest verbosity"
  },
  {
    command: "cat log | $0 -d '12-14 16:20'",
    describe: "Filter for messages within the minute of 16:20 on the 14th of december of any year."
  },
  {command: "cat log | $0 -d '03:21:45.895'", describe: "Filter for messages with exact local time match."}
];
