"use strict";

const {padLeft} = require("../services/padding");

const MAPPED = {}, PADDED = {};
const LIST = ["silent", "trace", "debug", "verbose", "info", "warn", "error", "fatal"];
const MAX_LENGTH = "verbose".length;

/*===================================================== Exports  =====================================================*/

exports.MAPPED = MAPPED;
exports.PADDED = PADDED;
exports.LIST = LIST;
exports.MAX_LENGTH = MAX_LENGTH;

/*================================================ Initial Execution  ================================================*/

for (let i = 0; i < LIST.length; i++) {
  MAPPED[LIST[i]] = i;
  PADDED[i] = padLeft(LIST[i].toUpperCase(), " ", MAX_LENGTH);
}
