"use strict";

const chalk = require("chalk");

/*===================================================== Exports  =====================================================*/

exports.LEVEL = {
  silent: (text) => chalk.dim(text),
  trace: (text) => chalk.gray(text),
  debug: (text) => chalk.green(text),
  verbose: (text) => chalk.cyan(text),
  info: (text) => chalk.blue(text),
  warn: (text) => chalk.yellow(text),
  error: (text) => chalk.red(text),
  fatal: (text) => chalk.bgRed(text)
};

exports.LINE = {_fallback: (text) => "  " + text}; // add one indentation level to block lines

exports.MESSAGE = {_fallback: (text) => chalk.bold(text)};
exports.NO_MESSAGE = {_fallback: (text) => chalk.bold(text)};
exports.ERROR_STACK = {_fallback: (text) => chalk.red(text)};
