/* eslint no-sync: "off" */
"use strict";

const fs = require("fs");
const path = require("path");

const LIST = ["default", "module"]; // make default first in list

/*===================================================== Exports  =====================================================*/

exports.LIST = LIST;

/*================================================ Initial Execution  ================================================*/

let files = fs.readdirSync(path.join(__dirname, "styles"));

for (let i = 0; i < files.length; i++) {
  let file = files[i];
  if (file.endsWith(".js") && file !== "default.js") { LIST.push(path.basename(file, ".js")); }
}
