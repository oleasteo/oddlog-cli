"use strict";

const {getLevelName, getPriorName} = require("../services/levels");

const SCHEMA_VERSION = 2;

/*===================================================== Classes  =====================================================*/

/**
 * [
 *   {Number}  SCHEMA_VERSION,
 *   {?String} typeKey,    // the key to use for type specifications within payload
 *   {?Array}  meta: [     // information about the system; can be overwritten by user
 *     {?String} platform, // usually the output of os.release()
 *     {?String} host,     // usually the output of os.hostname()
 *     {?String} pid       // usually the process.pid
 *   ],
 *   {String}  loggerName, // the name of the application (logger name)
 *   {String}  ISODate,    // the ISO formatted date string of the message
 *   {Number}  level,      // the message level
 *   {?Array}  source: [   // debugging information
 *     {String} file,      // source file that invoked the message logging method
 *     {Number} row,       // row within the file
 *     {Number} col        // col within the file
 *   ],
 *   {?String} message,    // the message text
 *   {?Object} [payload]   // the message payload as stringified JSON object
 * ]
 */
class MessageV2 {
  constructor(version, array, options) {
    this.version = version;
    this._verboseObject = null;
    this.typeKey = array[1];
    const metaArray = array[2];
    this.meta = metaArray && {platform: metaArray[0], host: metaArray[1], pid: metaArray[2]};
    this.name = array[3];
    this.date = new Date(array[4]);
    this.level = array[5];
    this.levelName = getLevelName(this.level);
    this.levelPrior = getPriorName(this.level);
    this.verbosity = options.verbosity[this.levelPrior];
    const source = array[6];
    this.source = source && {
      file: source[0],
      row: source[1],
      col: source[2],
    };
    this.message = array[7];
    this.payload = array[8];
  }

  getVerboseObject() {
    if (this._verboseObject != null) { return this._verboseObject; }
    let obj = {};
    obj.date = this.date;
    if (this.meta != null) { obj.meta = this.meta; }
    if (this.name != null) { obj.name = this.name; }
    obj.level = this.level;
    if (this.source != null) { obj.source = {file: this.source[0], row: this.source[1], col: this.source[2]}; }
    if (this.message != null) { obj.message = this.message; }
    if (this.payload !== void 0) { obj.payload = this.payload; }
    return this._verboseObject = obj;
  }
}

/*===================================================== Exports  =====================================================*/

module.exports = MessageV2;
module.exports.SCHEMA_VERSION = SCHEMA_VERSION;
