"use strict";

/*===================================================== Exports  =====================================================*/

exports.padLeft = padLeft;

/*==================================================== Functions  ====================================================*/

function padLeft(value, char, length) {
  value = value.toString();
  return repeat(char, length - value.length) + value;
}

function repeat(char, times) {
  if (times <= 0) { return ""; }
  char = char.toString();
  while (char.length < times) {
    char = char + char;
  }
  return char.length > times ? char.substr(0, times) : char;
}
